---
layout: post
title: Bootstrap
author: Frank
category: Learn
tags: Bootstrap
published: true
---
### 引用
<blockquote>
  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
  <footer>Someone famous in <cite title="Source Title">Source Title</cite></footer>
</blockquote>

<blockquote class="blockquote-reverse">
  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
  <footer>Someone famous in <cite title="Source Title">Source Title</cite></footer>
</blockquote>

### 小格式
  <del>This line of text is meant to be treated as deleted text.</del>  
  <u>This line of text will render as underlined</u>
  
  <small>This line of text is meant to be treated as fine print.</small>
  
  <strong>rendered as bold text</strong>
  
  <abbr title="attribute">attr</abbr>
  
  
### 对齐
<p class="text-left">Left aligned text.</p>
<p class="text-center">Center aligned text.</p>
<p class="text-right">Right aligned text.</p>
<p class="text-justify">Justified text.</p>
<p class="text-nowrap">No wrap text.</p>
### 两种列表方式
<ul class="list-inline">
  <li>横向内联1</li>
  <li>Phasellus iaculis</li>
  <li>Nulla volutpat</li>
</ul>
<ul>
  <li>a</li>
  <li>a</li>
  <li>a</li>
</ul>

* Red
* Green
* Blue

<ol>
  <li>1</li>
  <li>1</li>
  <li>1</li>
</ol>

1. w2
1. w1
1. w3

------
### 描述语
<dl>
  <dt>带有描述的短语列表。</dt>  <dd>全部内容全部部内容全部内容全部内容全部内容全部内容全部内容全部内容全部内容全部内容全部内容全部内容全部内容全部内容全部内容</dd>
</dl>

<dl class="dl-horizontal">
  <dt>水平排列的描述</dt>  <dd>全部内容全部部内容全部内容全部内容全部内容全部内容全部内容全部内容全部内容全部内容全部内容全部内容全部内容全部内容全部内容</dd>
</dl>

### 代码应用
For example, <code>&lt;section&gt;</code> should be wrapped as inline.

To switch directories, type <kbd>cd</kbd> followed by the name of the directory.<br>
To edit settings, press <kbd><kbd>ctrl</kbd> + <kbd>,</kbd></kbd>

<var>y</var> = <var>m</var><var>x</var> + <var>b</var>
<pre>&lt;p&gt;Sample text here...&lt;/p&gt;</pre>